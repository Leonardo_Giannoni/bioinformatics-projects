from Bio import SeqIO
import networkx as nx
import matplotlib.pyplot as plt

#dai reads iniziali li taglio in base al valore di Phred impostato
trimseq=[]
for record in SeqIO.parse('reads.fastq', 'fastq'):
    read_quals = record.letter_annotations['phred_quality']
    count = 0
    for i, q in enumerate(read_quals):
        if q <= 50:
            count += 1
            if count == 3:
                break
        elif count:
            count = 0
    if count != 3:
        trimseq.append(record)
    else:
        trimseq.append(record[:i-2])
    print ("Saved %i reads" % len(trimseq))

#salvataggio delle sequenze in un file fastq
output_handle=open('qtrim_seqs.fastq', 'w')
SeqIO.write(trimseq, output_handle, 'fastq')
output_handle.close()

#selezione delle basi senza la qualità, memorizzandole in un file di testo
with open('basi.txt', 'w') as f:
    for record in SeqIO.parse("qtrim_seqs.fastq", "fastq"):
     f.write("%s\n" % record.seq)
   
#apertura del file basi.txt
with open('basi.txt') as f:
    inp = f.read().splitlines()

#costruzione k-mers a partire dalla basi e dalla lunghezza del k-mers
def build_k_mer(str, k):
    edges = []
    for i in range(len(str) - k + 1):
        edges.append((str[i:i+k]))               
    return edges

#utilizzo della funzione build_k_mer settando la lunghezza dei k-mers a 31
reads=set()
k=31
for i in inp:
    edges = build_k_mer(i, k)
    b = list(edges)
    z=0
    while z<len(b) :
        reads.add(b[z])
        z=z+1

#creazione grafo de bruijn
def debruijn(reads):
    nodes = set()
    not_starts = set()
    edges = []
    for r in reads:
        r1 = r[:-1]
        r2 = r[1:]
        nodes.add(r1)
        nodes.add(r2)
        edges.append((r1,r2))
        not_starts.add(r2)
    return (nodes,edges,list(nodes-not_starts))

#creazione di una struttura che memorizza gli archi 
def make_node_edge_map(edges):
    node_edge_map = {}
    for e in edges:
        n = e[0]
        if n in node_edge_map:
            node_edge_map[n].append(e[1])
        else:
            node_edge_map[n] = [e[1]]
    return node_edge_map

#ricerca di un cammino euleriano all'interno del grafo, partendo da node_edge_map e da un nodo iniziale
def eulerian_path(m,v):
    nemap = m
    result_path = []
    start = v
    result_path.append(start)
    while(True):
        path = []
        previous = start
        while(True):
            if(previous not in nemap):
                break
            next = nemap[previous].pop()
            if(len(nemap[previous]) == 0):
                nemap.pop(previous,None)
            path.append(next)
            if(next == start):
                break;
            previous = next
        # completed one path
        #print(path)
        index = result_path.index(start)
        result_path = result_path[0:index+1] + path + result_path[index+1:len(result_path)]
        # choose new start
        if(len(nemap)==0):
            break
        found_new_start = False
        for n in result_path:
            if n in nemap:
                start = n
                found_new_start = True
                break # from for loop
        if not found_new_start:
        #    print("error")
        #    print("result_path",result_path)
        #    print(nemap)
            break
    return result_path

#assemblaggio del cammino euleriano trovato per ottenere la sequenza 
def assemble_path(path):
    if len(path) == 0:
        return ""
    result = path[0][:-1]
    for node in path:
        result += node[-1]
    return result

#visualizzazione del grafo di De Bruijn in formato sia grafico che testuale
def visualize_debruijn(G):
    P = nx.DiGraph()
    nodes = G[0]
    nodi = P.add_nodes_from(nodes) 
    edges = G[1]
    archi = P.add_edges_from(edges) 
    pos = nx.spring_layout(P)
    nx.draw_networkx_nodes(P, pos, nodelist=nodi,
                       node_color = 'w' , node_size = 100)
    nx.draw_networkx_labels(P, pos, font_size=2 )
    nx.draw_networkx_edges(P,pos,
                       edgelist= archi,
                       width=0.5,alpha=0.5,edge_color='b')
    plt.show()
    dot_str= 'digraph "DeBruijn graph" {\n '
    for node in nodes:
        dot_str += '    %s;\n' %(node)
    for src,dst in edges:
        dot_str += '    %s->%s;\n' %(src,dst)
    return dot_str + '}\n'

#utilizzo delle funzioni precedenti per creare la sequenza a partire da reads e k iniziali
def create_sequence(t,k):
    G = debruijn(reads)
    v = visualize_debruijn(G)
    print(v)
    nemap = make_node_edge_map(G[1])
    start = next(iter(G[2])) if (len(G[2]) > 0) else next(iter(G[0]))
    path = eulerian_path(nemap,start)
    return assemble_path(path)
    
#print della sequenza ottenuta 
sequenza=create_sequence(reads,k)
print(sequenza)

#salvataggio della sequenza finale in un file di testo
with open('sequenza.txt', 'w') as f:
     f.write("%s\n" % sequenza)

#salvataggio dei nodi e degli archi in un file di testo
def save_nodes_edges(t,k):
    G = debruijn(reads)
    nodes = G[0]
    edges = G[1]
    with open('OutputListaNodi.txt', 'w') as f:
     f.write("%s\n" % nodes)
    with open('OutputListaArchi.txt', 'w') as f:
     f.write("%s\n" % edges)   
  
output = save_nodes_edges(reads,k)